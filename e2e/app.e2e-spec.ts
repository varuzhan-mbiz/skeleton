import { OfficialTutorialPage } from './app.po';

describe('official-tutorial App', function() {
  let page: OfficialTutorialPage;

  beforeEach(() => {
    page = new OfficialTutorialPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
