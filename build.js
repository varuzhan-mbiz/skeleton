var writeFile = require('write');
var exec = require('child_process').exec;

var allowedKeys = {
    baseUrl: 'base-url'
};

var config = {
    'production': true,
    'login-url':   '',
    'base-url':    '',
    'app-base-url': ''
};

var command = 'ng build --env=config --target=production';

var params = process.argv;
params = params.splice(2, params.length);
params = params.map( function(param){
    var parts = param.split( '=' );
    return { param: parts[0], value: parts[1] };
} );

writeFile( 'src/environments/environment.config.ts', generateEnvFileContent(params) );
executebuildCommand();

function generateEnvFileContent( params ) {

    var prefix = 'export const environment = ';

    params.forEach( function( item ){ config[ item.param ] = item.value; } );

    return prefix + JSON.stringify(config, null, 2) + ';';

}

function executebuildCommand(){
    exec( command,{ maxBuffer: 1024 * 1024 }, function(err, stdout, stderr){

        err    && console.error(err);
        stdout && console.log(stdout);
        stderr && console.error(stderr);

    } );
}