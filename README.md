# @microbizllc/skeleton

This project was generated with [@angular/cli](https://github.com/angular/angular-cli) version 1.0.0-rc.1
##What is included?

- Tools and Components
    - [Angular Material 2](https://github.com/angular/material2) - Material Design components for Angular apps.   
    - [Kendo UI for Angular](http://www.telerik.com/kendo-angular-ui/) - Professional Grade Angular 2 UI Components.        
    - [SCSS](http://sass-lang.com/) - Is the most mature, stable, and powerful professional grade CSS extension 
    language in the world.
    
- Dev Tools    
    - [Karma](https://karma-runner.github.io) - Brings a productive testing environment to developers. 
    - [Protractor](http://www.protractortest.org/) - Is an end-to-end test framework for AngularJS applications. 

##Getting started
1. Copy "package-<APP-NAME>.json.dist" to "package.json"

    `cp package-application.json.dist package.json`
2. Copy "./src/app/config/index-<APP-NAME>.ts.dist" to "./src/app/config/index.ts"

    `cp ./src/app/config/index-application.ts.dist ./src/app/config/index.ts`

3 `npm login --registry=https://registry.npm.telerik.com/ --scope=@progress`: Enable the Progress NPM registry on your 
machine by associating the @progress scope with the registry URL. 
Run the following command in your terminal:

    > Note: this is needed by Kendo-UI [More Details](http://www.telerik.com/kendo-angular-ui/getting-started/)

Enter the username (if the username is an email address, use everything before the @) and password you use to log in 
your Telerik account as NPM will ask you for your Telerik account credentials and an email. 
The login name will be ignored.
    
4. For server config create .npmrc file inside root folder, 

    Manually log in to @progress scope ( follow step 3 )    
    
    After login look inside ~/.npmrc file 
    
    Copy token from file and paste in your local .npmrc file
    
    `@progress:registry=https://registry.npm.telerik.com/
     //registry.npm.telerik.com/:_authToken=<paste-token-here>`


 
5. `npm install`

6.  `npm run build app-base-url=<app-base-url> base-url=<base-url> login-url=<login-url>`
    
    Example: `npm run build app-base-url=/skeleton-new/dist base-url=http://api.microbiz.loc login-url=/oauthdfgdfg`:


    

> Note: You may want angular-cli to be global 
>
> `npm install angular-cli -g` 

##What developers need?
- [Angury](https://augury.angular.io/) - Augury is the most used Google Chrome Developer Tool extension for debugging 
and profiling Angular 2 applications.
- [Redux DevTools Extension](http://zalmoxisus.github.io/redux-devtools-extension/) - A live-editing time travel 
environment for Redux.

## Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change 
any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. 
You can also use `ng generate directive/pipe/service/class`.

> Note: Components prefix is set to **`mbiz`**

## Build
```
$ npm run build api-url="<your-api-url>"
 ```
 
The build artifacts will be stored in the `dist/` directory. 


## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Deploying to Github Pages

Run `ng github-pages:deploy` to deploy to Github Pages.

## Further help

To get more help on the `angular-cli` use `ng --help` or go check out the 
[Angular-CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
