import { BrowserModule }   from '@angular/platform-browser';
import { NgModule,
         ErrorHandler }    from '@angular/core';
import { FormsModule }     from "@angular/forms";
import { HttpModule }      from "@angular/http";
import { APP_BASE_HREF }   from '@angular/common';
import { DialogService, DialogModule } from "@progress/kendo-angular-dialog";

import { AppComponent }    from './app.component';
import { MODULES }         from "./config/index";
import { AppRoutingModule } from "./app-routing.module";

import { environment } from "../environments/environment";

export class GlobalErrorHandler implements ErrorHandler {
    handleError( error ) {
        console.trace( error.stack );
    }
}

@NgModule( {
    declarations: [
        AppComponent,
    ],
    imports:      [
        BrowserModule,
        FormsModule,
        HttpModule,
        AppRoutingModule,
        DialogModule,
        MODULES
    ],
    providers:    [
        {provide: APP_BASE_HREF, useValue:environment['app-base-url']},
        //{ provide: ErrorHandler, useClass: GlobalErrorHandler },
        DialogService
    ],
    bootstrap:    [ AppComponent ]
} )
export class AppModule {
}



