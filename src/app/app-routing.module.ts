import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";

import { REDIRECT_TO } from './config/index';

@NgModule( {
    imports: [
        RouterModule.forRoot( [ { path: '', redirectTo: REDIRECT_TO, pathMatch: 'full' } ] )
    ],
    exports: [ RouterModule ]
} )
export class AppRoutingModule {}