import { Component } from '@angular/core';
import { DialogService } from "@progress/kendo-angular-dialog";
import { ROUTES } from './config/index';


@Component( {
    selector:    'app-root',
    templateUrl: './app.component.html',
    styleUrls:   [ './app.component.scss' ]
} )
export class AppComponent {

    constructor( private dialogService: DialogService ) {

        this.initChildRoutes();

    }

    childRoutes;

    initChildRoutes() {

        this.childRoutes = ROUTES.map( childRoutes => childRoutes.map( route => route.path ) )
            .reduce( ( acc, childRoutes ) => {
                acc.push( childRoutes[ 0 ] );
                return acc;
            }, [] );

    }

}

